# Automation 1 
# ===============================compress =======================================
# read -e -p "Log Directory: " log_directory
# read -e -p "File Extension: " extension
# read -e -p "Backup Directory: " backup_directory
# tar czf archive.tar.gz $(find $log_directory -name "*.$extension")
# mv archive.tar.gz $backup_directory/$(date +%F).tar.gz
# rm $(find $log_directory -name "*.$extension")
# exit 0 


# Automation 2 
# ===============================Install Lamb=======================================
# echo -e "\n\nUpdating Apt Packages and upgrading latest patches\n"
# sudo apt-get update -y && sudo apt-get upgrade -y
#  
# echo -e "\n\nInstalling Apache2 Web server\n"
# sudo apt-get install apache2 apache2-doc apache2-mpm-prefork apache2-utils libexpat1 ssl-cert -y
#  
# echo -e "\n\nInstalling PHP & Requirements\n"
# sudo apt-get install libapache2-mod-php7.0 php7.0 php7.0-common php7.0-curl php7.0-dev php7.0-gd php-pear php7.0-mcrypt php7.0-mysql -y
#  
# echo -e "\n\nInstalling MySQL\n"
# sudo apt-get install mysql-server mysql-client -y
#  
# echo -e "\n\nPermissions for /var/www\n"
# sudo chown -R www-data:www-data /var/www
# echo -e "\n\n Permissions have been set\n"
#  
# echo -e "\n\nEnabling Modules\n"
# sudo a2enmod rewrite
# sudo phpenmod mcrypt
#  
# echo -e "\n\nRestarting Apache\n"
# sudo service apache2 restart
#  
# echo -e "\n\nLAMP Installation Completed"
#  
# exit 0


# Automation 3 
# =============================== Formation the Terminal=======================================
# style() {
#     echo "\033[$1m"
# }
#
# red_text(){ printf "$(style "31")$1$(style "0")"; }
# green_text(){ printf "$(style "32")$1$(style "0")"; }
# yellow_text(){ printf "$(style "33")$1$(style "0")"; }
# blue_text(){ printf "$(style "34")$1$(style "0")"; }
# cyan_text(){ printf "$(style "35")$1$(style "0")"; }
#
# progress_bar() {
#     if [ -z "$1" ] ; then
#         echo "Error: The duration of the progress bar is required" >&2
#     fi
#
#     local duration=$1
#     local increment=$((100/$duration))
#     for (( elapsed=0; elapsed<=100; elapsed=elapsed+increment )); do
#         for ((done=0; done<elapsed; done=done+1)); do
#             printf "$(green_text "▇")";
#         done
#
#         for ((remain=elapsed; remain<100; remain=remain+1)); do
#             printf "$(cyan_text "-")";
#         done
#
#         printf "| $(yellow_text "$elapsed") %%";
#         sleep 1
#         printf "\r"
#     done
#
#     echo -e "\n";
# }
#
# camelcase() {
#     echo $1 | awk '{print toupper(substr($0,0,1))tolower(substr($0,2))}'
# }
#


# Automation 4 
# =============================== Formation the Utils=======================================
declare -r TRUE=0
declare -r FALSE=1

die(){
    echo "$1" >&2;
    exit 1
}

has_root_permission(){
    [ $(id -u) -eq 0 ] && return $TRUE || return $FALSE
}

