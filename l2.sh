# #IF ELSE
# number1=5;
# number2=5;
#
# if [ $number1 -eq $number2 ]; then
#     echo "$number1 is equal to $number2"
# else
#     echo "$number1 is not equal to $number2"
# fi
#
# #LOOP
# names="Name1 Name2 Name3"
#
# for name in names; do
#     echo "Hello $name"
# done
#
# for number in `seq 1 10`; do
#     echo "I am $number" 
#     # sleep 2
# done
#
# #FUNCTION
# function greet {
#     echo "Hello $1!"
# }
# greet "Hai"
# greet "user"


# readonly greeting="Hello"
# echo "Hi, I am $(whoami)."
#
# read -e -p "Who are you? " saya
# current_time= `date +%X`
# echo "${greeting:-Hi}, $saya. Now the time is $current_time"

# number1=5; number2=5
#
# if [ $number1 -gt $number2 ]; then
#     echo "$number1 is greater than $number2"
# elif [ $number1 -lt $number2 ]; then
#     echo "$number1 is lesser than $number2"
# else
#     echo "$number1 is equal to $number2"
# fi


# number1=5; 
# number2=15;
#     
# if [ $number1 -ne $number2 ]; then
#     if [ $number1 -gt $number2 ]; then
#         echo "$number1 is greater than $number2"
#     else
#         echo "$number1 is lesser than $number2"
#     fi
#
# else
#     echo "$number1 is equal to $number2"
#
# fi


# echo "any standart text can be here"
# switch
# name="alexa"
# case "$name" in
#     john) echo "Welcome Admin";;
#     alexa) echo "Welcome User" ;;
#
#     *) echo "Access Denied" ;;
# esac

# myName="User!"
# echo "Hello ${myName}"


# Whileloop
# count="9"
# while [ $count -gt 0 ]; do
#     echo "$count"
#     count=$[$count-1]
# done










# Download file
# a=20
# while [ $a -lt 40 ]
# do
# 	# wget "https://nptel.ac.in/courses/111104075/pdf/Module1/Lecture$a-Module1-Anova-1.pdf"
# 	wget "https://www.irs.gov/pub/irs-pdf/fw8ben.pdf"
# 	a=$(($a+1))
# done

# Echo script ls -la
# echo "List of folder"
# ls -ltu /home/syam/Desktop/
# read -p "Enter now: " f 
# echo $f

# Command input
# read -p "Enter this var: " f
# if [ $f -eq 2 ]
# then
#     echo "Install mailx"
#     exit 1
#
# fi

# read var                                                          
# if [ $(whoami) == "root" ]
# then 
#     sleep 3
#     grep -n $var /home/*/.bash_history >> file.txt
#     cat file.txt
# else
#     echo "Run as root"
#     exit
# fi



